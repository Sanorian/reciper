from django.db import models

# Create your models here.
class User(models.Model):
    email=models.EmailField(max_length=254)
    password=models.CharField(max_length=50)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Recipe(models.Model):
    name = models.CharField(max_length=200)
    text = models.TextField()
    rating = models.IntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name